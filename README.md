# sgo

#### 介绍
Go 使用中的一些定制功能（现在有的是日志格式定制）

#### 安装教程

1.  export GOPRIVATE="gitee.com"
2.  go get -u gitee.com/snowtrace-chui/sgo
3.  go get -u github.com/fatih/color

#### 使用说明

1. 使用log模块
    import "gitee.com/snowtrace-chui/sgo/log"
2. 使用redis模块
   import "gitee.com/snowtrace-chui/sgo/redis"
