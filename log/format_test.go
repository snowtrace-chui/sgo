/**
 * @Author: grainbuds
 * @Created Date: 2023年03月26日 下午3:05 星期日
 * @Description: desc
 */
package log

import (
	"testing"
)

func TestLogger(t *testing.T) {
	logger := NewLog("debug", "")
	Export(logger)
	defer logger.Close()

	Debug("测试")
	Trace("追踪")
	Alert("警告")
	Error("错误")
	Fatal("致命错误")
}
