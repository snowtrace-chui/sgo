/**
 * @Author: grainbuds
 * @CreateDate: 2023-08-07 11:32:09 星期一
 * @Description: api_test.go
 */
package redis

import (
	"github.com/gogf/gf/util/gconv"
	redigo "github.com/gomodule/redigo/redis"
	"github.com/stretchr/testify/require"
	"os"
	"testing"
)

func TestRedisCallApi(t *testing.T) {
	scriptData, err := os.ReadFile("./api.lua")
	if err != nil {
		t.Logf("ReadFile err:%+v", err)
		return
	}
	conn := client.pool.Get()
	_, err = conn.Do("PING")
	require.NoError(t, err)

	GetClusterRedisClient().ScriptFlush()
	sha1Sign, err := GetClusterRedisClient().ScriptLoad(string(scriptData))
	if err != nil {
		t.Logf("ScriptLoad err: %+v", err.Error())
		return
	}

	rets, err := redigo.Strings(GetClusterRedisClient().EvalSha(sha1Sign, gconv.Strings("TestZSet"), "ZSetValue1"))
	if err != nil {
		t.Logf("GainData err: %+v", err.Error())
	}
	t.Logf("GainData ret: %+v", rets)
}
