-- redis lua api
-- redis-cli> eval "local t = redis.call('type', KEYS[1]); for k, v in pairs(t) do return {k, v} end" 1 key
local table_unpack = unpack or table.unpack
local table_insert = table.insert

local kType = redis.call('type', KEYS[1])
local argNum = #ARGV

if kType['ok'] == 'none' then
	return nil --Go中得到的是 err = redis.Nil
elseif kType['ok'] == 'string' then

elseif kType['ok'] == 'hash' then

elseif kType['ok'] == 'list' then

elseif kType['ok'] == 'set' then

elseif kType['ok'] == 'zset' then
	if argNum == 1 then
		local rank = redis.call('zrank', KEYS[1], ARGV[1])
		if rank == nil then
			return {}
		end
		local score = redis.call('zscore', KEYS[1], ARGV[1])
		return {tostring(rank), score}
	else
		local paramsList = {}
    	table_insert(paramsList, '0')           -- start
    	table_insert(paramsList, '100')         -- stop
    	table_insert(paramsList, 'WITHSCORES')  -- show score
    	return redis.call('zrange', KEYS[1], table_unpack(paramsList))
	end
end

return {}